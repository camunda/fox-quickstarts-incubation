package com.camunda.fox.quickstart.processvariants;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.impl.ProcessDefinitionQueryImpl;
import org.activiti.engine.impl.context.Context;

/**
 * Use for expression for call activity:
 * processKeySelector.getProcessKeyForSubprocess('subprocesskey')
 */
@Named
public class ProcessKeySelector {

  @Inject
  @Named
  private Map<String, Object> processVariables;

  public String getProcessKeyForSubprocess(String subProcessDefinitionKey, String tenantId) {
    // get current tenant
    String tenant = (String) processVariables.get("TENANT_ID");
    System.out.println("### " + tenant + " =? " + tenantId);

    String tenantSpecificSubProcessDefinitionKey = subProcessDefinitionKey + "-" + tenantId;

    // search for tenant specific process definition
    ProcessDefinitionQueryImpl processDefinitionQuery = new ProcessDefinitionQueryImpl().processDefinitionKey(tenantSpecificSubProcessDefinitionKey)
            .latestVersion();
    long count = Context.getCommandContext().getProcessDefinitionManager().findProcessDefinitionCountByQueryCriteria(processDefinitionQuery);

    if (count > 0) {
      // if tenant specific process definition is found use this as key
      System.out.println("Using sub process: " + tenantSpecificSubProcessDefinitionKey + " / " + tenantId);
      return tenantSpecificSubProcessDefinitionKey;
    } else {
      // otherwise use the default process definition
      System.out.println("Using sub process: " + subProcessDefinitionKey);
      return subProcessDefinitionKey;
    }
  }

}
