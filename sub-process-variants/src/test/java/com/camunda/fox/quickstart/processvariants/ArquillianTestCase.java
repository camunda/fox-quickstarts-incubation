package com.camunda.fox.quickstart.processvariants;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArquillianTestCase {

  @Deployment  
  public static WebArchive createDeployment() {
    MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom("pom.xml");
     
    return ShrinkWrap.create(WebArchive.class, "quickstart-sub-process-variants.war")            
            .addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAsFiles())
            .addAsWebResource("META-INF/test-processes.xml", "WEB-INF/classes/META-INF/processes.xml")
            .addAsWebResource("META-INF/beans.xml", "WEB-INF/classes/META-INF/beans.xml")
            
            .addPackages(false, "com.camunda.fox.quickstart.processvariants")
            
            .addAsResource("sub-process-variants.bpmn");
  }

  @Inject
  private RuntimeService runtimeService;

  @Inject
  private TaskService taskService;


  @Test
  public void test() throws Exception {
    List<Task> tasks = null;
    ProcessInstance processInstance = null;
    
    // Tenant with default process definition
    HashMap<String, Object> variables1 = new HashMap<String, Object>();  
    variables1.put("TENANT_ID", "tenant1");
    
    processInstance = runtimeService.startProcessInstanceByKey("sub-process-variants-main", variables1);    
    assertThat(runtimeService.getActiveActivityIds(processInstance.getId()), hasItem("call-activity"));
    tasks = taskService.createTaskQuery().processDefinitionKey("sub-process-variants-tenant1").list();
    assertEquals(1, tasks.size());
    assertEquals("do A", tasks.get(0).getName());

    // Tenant with default process definition
    HashMap<String, Object> variables2 = new HashMap<String, Object>();  
    variables2.put("TENANT_ID", "tenant2");
    
    processInstance = runtimeService.startProcessInstanceByKey("sub-process-variants-main", variables2);    
    assertThat(runtimeService.getActiveActivityIds(processInstance.getId()), hasItem("call-activity"));
    tasks = taskService.createTaskQuery().processDefinitionKey("sub-process-variants-tenant2").list();
    assertEquals(1, tasks.size());
    assertEquals("do B", tasks.get(0).getName());

    // Tenant with default process definition
    HashMap<String, Object> variables3 = new HashMap<String, Object>();  
    variables3.put("TENANT_ID", "tenant3");
    
    processInstance = runtimeService.startProcessInstanceByKey("sub-process-variants-main", variables3);    
    assertThat(runtimeService.getActiveActivityIds(processInstance.getId()), hasItem("call-activity"));
    tasks = taskService.createTaskQuery().processDefinitionKey("sub-process-variants").list();
    assertEquals(1, tasks.size());
    assertEquals("do X", tasks.get(0).getName());
  }
}
