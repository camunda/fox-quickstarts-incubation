package com.camunda.fox.quickstart.callactivity.parameters;

import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;

public class PassingParametersTestCase extends ActivitiTestCase {

  @Deployment(resources = "SomeProcess.bpmn")
  public void testParsingAndDeployment() {
  }
  
  @Deployment(resources = "SomeProcess.bpmn")
  public void testParseListenersUsed() {
    // TODO: How can we test this correctly on the fox platform automatically
    runtimeService.startProcessInstanceByKey("some-process");

  }
  
}