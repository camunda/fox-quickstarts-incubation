package com.camunda.fox.quickstart.platform.parselistener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.impl.bpmn.parser.BpmnParseListener;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import com.camunda.fox.platform.impl.configuration.JtaCmpeProcessEngineConfigurationFactory;


public class QuickstartProcessEngineConfigurationFactory extends JtaCmpeProcessEngineConfigurationFactory {

  @Override
  public ProcessEngineConfigurationImpl getProcessEngineConfiguration() {
    ProcessEngineConfigurationImpl processEngineConfiguration = super.getProcessEngineConfiguration();
    
    // normally no parse listeners should be set, so create an own list for it
    if (processEngineConfiguration.getPostParseListeners()==null) {
      processEngineConfiguration.setPostParseListeners(new ArrayList<BpmnParseListener>());
    }
        
    // Change whatever you want to change in the configuration, see 
    // https://app.camunda.com/confluence/display/foxUserGuide/Extending+the+configuration+of+the+fox+platform#Extendingtheconfigurationofthefoxplatform-UseCases
    // for some typical use cases
    processEngineConfiguration.getPostParseListeners().add( new QuickstartParseListener() );
    
    return processEngineConfiguration;
  }

}
