package com.camunda.fox.quickstart.platform.parselistener;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.activiti.engine.impl.bpmn.parser.AbstractBpmnParseListener;
import org.activiti.engine.impl.bpmn.parser.BpmnParseListener;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ScopeImpl;
import org.activiti.engine.impl.util.xml.Element;


public class QuickstartParseListener extends AbstractBpmnParseListener implements BpmnParseListener {
  
  public static int serviceTaskCounter = 0;
  
  private static Logger log = Logger.getLogger(QuickstartParseListener.class.getName());

  @Override
  public void parseServiceTask(Element serviceTaskElement, ScopeImpl scope, ActivityImpl activity) {
    super.parseServiceTask(serviceTaskElement, scope, activity);
    serviceTaskCounter++;
    log.log(Level.WARNING, "Found new service task in process: " + activity.getId());
  }

}
