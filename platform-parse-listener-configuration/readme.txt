# Introduction
This quickstart shows how to extend the fox engine configuration in the fox platform.
See https://app.camunda.com/confluence/display/foxUserGuide/Extending+the+configuration+of+the+fox+platform
for more details on this.

# Environment Restrictions

# Remarks to run this quickstart
This quickstart must be manually "installed" on your fox-platform depending on the application
server you use. Please refer to 
https://app.camunda.com/confluence/display/foxUserGuide/Extending+the+configuration+of+the+fox+platform
for more details.

# Known Issues / Todo:
- The quickstart does not yet have an automated test case, where the platform extension is 
  automatically installed in at least one application server