# Introduction
This quickstart shows to call a WebService using Apache CXF from within athe fox-engine using a ServiceTask. 

# Environment Restrictions
The quickstart requires CXF to be part of the Application Server. *We only tested this on JBoss 7.1*.

# Remarks to run this quickstart
There is no web interface to access the application, if it deploys everything is fine. In order to test anything please refer to the Arquillian test case in the sources.