<xsl:stylesheet version="2.0" xmlns:saxon="http://icl.com/saxon" 
							  xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" 
							  xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" 
							  xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" 
							  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
							  xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" 
							  xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL"
							  >
<xsl:output method="text" encoding="UTF-8" />

<!-- some functions we use, may be obsolete in the meantime? Copied from an older project example -->
<xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>

<xsl:template name='firstLetterUpperCase'>
	<xsl:param name='toconvert' />
	<xsl:value-of select="concat(  translate(  substring($toconvert, 1,1)  ,$lcletters,$ucletters),  substring($toconvert, 2)  )"/>
</xsl:template>

<xsl:variable name="nokletters">? Ã¶Ã¤Ã¼.()-+</xsl:variable>
<xsl:variable name="okletters">__oau_____</xsl:variable>

<xsl:template name='convertName'>
	<xsl:param name='toconvert' />
	<xsl:value-of select="translate( $toconvert ,$nokletters, $okletters)"/>
</xsl:template>

<xsl:template name='upperCase'>
	<xsl:param name='toconvert' />
	<xsl:value-of select="translate( $toconvert ,$lcletters, $ucletters)"/>
</xsl:template>

<!-- Do it -->

<xsl:template match="bpmn:definitions">
    <xsl:apply-templates />    
</xsl:template>

<xsl:template match="bpmn:process">
    <xsl:variable name="idValid"><xsl:call-template name="convertName"><xsl:with-param name="toconvert"><xsl:value-of select="@id" /></xsl:with-param></xsl:call-template></xsl:variable>
    <xsl:variable name="idCamelCase"><xsl:call-template name="firstLetterUpperCase"><xsl:with-param name="toconvert"><xsl:value-of select="$idValid" /></xsl:with-param></xsl:call-template></xsl:variable>
    <xsl:variable name="idUpperCase"><xsl:call-template name="upperCase"><xsl:with-param name="toconvert"><xsl:value-of select="$idValid" /></xsl:with-param></xsl:call-template></xsl:variable>
public static class <xsl:value-of select="$idCamelCase" /> { 
    public static transient final String PROCESS_<xsl:value-of select="idUpperCase" />_NAME = "<xsl:value-of select="@name" />";
    public static transient final String PROCESS_<xsl:value-of select="idUpperCase" />_KEY = "<xsl:value-of select="@id" />";
    <xsl:apply-templates />
}
</xsl:template>

<xsl:template name='flowNode'>
	<xsl:param name='id' />
	<xsl:param name='name' />
	<xsl:param name='type' />
    <xsl:variable name="idValid"><xsl:call-template name="convertName"><xsl:with-param name="toconvert"><xsl:value-of select="$id" /></xsl:with-param></xsl:call-template></xsl:variable>
    <xsl:variable name="idCamelCase"><xsl:call-template name="firstLetterUpperCase"><xsl:with-param name="toconvert"><xsl:value-of select="$idValid" /></xsl:with-param></xsl:call-template></xsl:variable>
    <xsl:variable name="idUpperCase"><xsl:call-template name="upperCase"><xsl:with-param name="toconvert"><xsl:value-of select="$idValid" /></xsl:with-param></xsl:call-template></xsl:variable>    
    public static transient final String FLOWNODE_<xsl:value-of select="$type" />_<xsl:value-of select="$idUpperCase" />_ID = "<xsl:value-of select="@id" />";    
    public static transient final String FLOWNODE_<xsl:value-of select="$type" />_<xsl:value-of select="$idUpperCase" />_NAME = "<xsl:value-of select="@name" />";    
</xsl:template>

<xsl:template match="bpmn:startEvent">
    <xsl:call-template name="flowNode">
    	<xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="type">START_EVENT</xsl:with-param>
    </xsl:call-template>
</xsl:template>
<xsl:template match="bpmn:endEvent">
    <xsl:call-template name="flowNode">
    	<xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="type">END_EVENT</xsl:with-param>
    </xsl:call-template>
</xsl:template>
<xsl:template match="bpmn:intermediateThrowEvent">
    <xsl:call-template name="flowNode">
    	<xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="type">INTERMEDIATE_EVENT</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="bpmn:userTask">
    <xsl:call-template name="flowNode">
    	<xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="type">USER_TASK</xsl:with-param>
    </xsl:call-template>
</xsl:template>
<xsl:template match="bpmn:serviceTask">
    <xsl:call-template name="flowNode">
    	<xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="type">SERVICE_TASK</xsl:with-param>
    </xsl:call-template>
</xsl:template>
<xsl:template match="bpmn:receiveTask">
    <xsl:call-template name="flowNode">
    	<xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="type">RECEIVE_TASK</xsl:with-param>
    </xsl:call-template>
</xsl:template>
<xsl:template match="bpmn:businessRuleTask">
    <xsl:call-template name="flowNode">
    	<xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
		<xsl:with-param name="name"><xsl:value-of select="@name" /></xsl:with-param>
		<xsl:with-param name="type">BUSINESS_RULE_TASK</xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template match="bpmn:exclusiveGateway" />
<xsl:template match="bpmn:inclusiveGateway" />
<xsl:template match="bpmn:parallelGateway" />

<xsl:template match="bpmn:sequenceFlow" />

<!-- TODO: sub processes -->

</xsl:stylesheet>